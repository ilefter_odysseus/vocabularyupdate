1. Create source table
2. Download "Full Set" (http://loinc.org/downloads/files/loinc-table-csv-text-format/loinc-table-file-csv-text-format/download)
	https://loinc.org/downloads/loinc/
3. Download "Multiaxial Hierarchy" (http://loinc.org/downloads/files/loinc-multiaxial-hierarchy/loinc-multiaxial-hierarchy-file/download)
	https://loinc.org/download/loinc-table-file-csv/
4. Extract: loinc.csv, map_to.csv, source_organization.csv, LOINC_250_MULTI-AXIAL_HIERARCHY.CSV
   (LOINC_2.56_Text.zip: loinc.csv, map_to.csv, source_organization.csv; 
    LOINC_2.56_MULTI-AXIAL_HIERARCHY.zip: LOINC_2.56_MULTI-AXIAL_HIERARCHY.CSV)
5. Load data from the "loinc.csv", "map_to.csv", "source_organization.csv" and "LOINC_HIERARCHY" with help of "LOINC.CTL", "MAP_TO.CTL", "SOURCE_ORGANIZATION.CTL" and "loinc_hierarchy.ctl" 
	into the tables: LOINC, MAP_TO, SOURCE_ORGANIZATION, LOINC_HIERARCHY.
6. Download "Answers and Forms" (http://loinc.org/downloads/files/loinc-panels-and-forms-file/loinc-panels-and-forms-file-all-selected-panels-and-forms/download)
7. Extract "LOINC_256_PanelsAndForms.xlsx" from the "LOINC_2.56_PanelsAndForms.zip"
8. Open the LOINC_XXX_PanelsAndForms.xlsx, save data from "ANSWERS" worksheet to the "loinc_answers.txt" file, 
   save data from the "FORMS" worksheet to the "loinc_forms.txt" file.
9. Load data from the "loinc_answers.txt" and "loinc_forms.txt" with help of "LOINC_ANSWERS.CTL" and "loinc_forms.ctl" into the tables LOINC_ANSWERS, LOINC_FORMS.
10. Load data from the "loinc_class.csv" with help of "loinc_class.ctl" into the table "table loinc_class".
11. Download "Expression Association" (https://loinc.org/news/draft-loinc-snomed-ct-mappings-and-expression-associations-now-available.html)
12. Extract "\RF2Release\Full\Refset\Content\xder2_sscccRefset_LOINCExpressionAssociationFull_INT_xxxxxxxx.txt" and rename to "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.txt"
13. Load data from "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.txt" with help of "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.ctl" into the table "scccRefset_MapCorrOrFull_INT"
14. Download "CPT Mappings" (http://www.nlm.nih.gov/research/umls/mapping_projects/loinc_to_cpt_map.html)
15. Extract the "MRSMAP.RRF" file
16. Load data from the "MRSMAP.RRF" with help of "CPT_MRSMAP.ctl" into the table CPT_MRSMAP
17. Run "load_stage.sql"
18. Run "generic_update.sql"
19. Run "Check Update Results"

Optimized:
Download phase:
1 (2). Download "Full Set" (http://loinc.org/downloads/files/loinc-table-csv-text-format/loinc-table-file-csv-text-format/download) - LOINC_2.56_Text.zip
	https://loinc.org/download/loinc-table-file-csv/
	https://loinc.org/wp-login.php?redirect_to=/download/loinc-table-file-csv/
	https://loinc.org/file-access/download-id/8960/

2 (3). Download "Multiaxial Hierarchy" (http://loinc.org/downloads/files/loinc-multiaxial-hierarchy/loinc-multiaxial-hierarchy-file/download) - LOINC_2.56_MULTI-AXIAL_HIERARCHY.zip
	
3 (6). Download "Answers and Forms" (http://loinc.org/downloads/files/loinc-panels-and-forms-file/loinc-panels-and-forms-file-all-selected-panels-and-forms/download) - LOINC_2.56_PanelsAndForms.zip
4 (11). Download "Expression Association" (https://loinc.org/news/draft-loinc-snomed-ct-mappings-and-expression-associations-now-available.html) - SnomedCT_LOINC_AlphaPhase3_INT_20160401.zip
5 (14). Download "CPT Mappings" (http://www.nlm.nih.gov/research/umls/mapping_projects/loinc_to_cpt_map.html) - LNC215_TO_CPT2005_MAPPINGS.zip

Prepare phase:
6 (4). Extract: loinc.csv, map_to.csv, source_organization.csv, LOINC_250_MULTI-AXIAL_HIERARCHY.CSV
   (LOINC_2.56_Text.zip: loinc.csv, map_to.csv, source_organization.csv; 
    LOINC_2.56_MULTI-AXIAL_HIERARCHY.zip: LOINC_2.56_MULTI-AXIAL_HIERARCHY.CSV)
7 (7). Extract "LOINC_256_PanelsAndForms.xlsx" from the "LOINC_2.56_PanelsAndForms.zip"
8 (8). Open the LOINC_XXX_PanelsAndForms.xlsx, save data from "ANSWERS" worksheet to the "loinc_answers.txt" file, 
   save data from the "FORMS" worksheet to the "loinc_forms.txt" file.
9 (12). Extract "\RF2Release\Full\Refset\Content\xder2_sscccRefset_LOINCExpressionAssociationFull_INT_xxxxxxxx.txt" and rename to "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.txt"
10 (15). Extract the "MRSMAP.RRF" file

Load data phase:
11 (5). Load data from the "loinc.csv", "map_to.csv", "source_organization.csv" and "LOINC_HIERARCHY" with help of "LOINC.CTL", "MAP_TO.CTL", "SOURCE_ORGANIZATION.CTL" and "loinc_hierarchy.ctl" 
		into the tables: LOINC, MAP_TO, SOURCE_ORGANIZATION, LOINC_HIERARCHY.
12 (9). Load data from the "loinc_answers.txt" and "loinc_forms.txt" with help of "LOINC_ANSWERS.CTL" and "loinc_forms.ctl" into the tables LOINC_ANSWERS, LOINC_FORMS.
13 (10). Load data from the "loinc_class.csv" with help of "loinc_class.ctl" into the table "table loinc_class".
14 (13). Load data from "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.txt" with help of "xder2_sscccRefset_LOINCExpressionAssociationFull_INT.ctl" into the table "scccRefset_MapCorrOrFull_INT"
15 (16). Load data from the "MRSMAP.RRF" with help of "CPT_MRSMAP.ctl" into the table CPT_MRSMAP

Data processign phase:
16 (17). Run "load_stage.sql"
17 (18). Run "generic_update.sql"

Verification phase:
18 (19). Run "Check Update Results"

